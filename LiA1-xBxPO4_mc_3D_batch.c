// MC simulation of magnetic disorder in LiA(1-x)BxPO4

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>


// Define parameters
#define L 10		// Length of simulation cube in unit cells

// Positions for 3D, order: 4, 1, 3, 2
double dx[4] = {0.225,0.275,0.725,0.775};
double dy[4] = {0.75,0.25,0.75,0.25};
double dz[4] = {0.48,-0.02,0.02,0.52};

//Some parameters
//double beta[33] = {0.1,0.2,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1,1.05,1.1,1.15,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,3.0,4.0,5.0,10.0};
//double beta[7] = {2.5,3.5,4.5,6.0,7.0,8.0,9.0};
//double beta[60] = {0.1934,0.1967,0.2001,0.2036,0.2072,0.2110,0.2149,0.2190,0.2232,0.2275,0.2321,0.2368,0.2418,0.2469,0.2523,0.2579,0.2637,0.2699,0.2763,0.2830,0.2901,0.2976,0.3054,0.3136,0.3224,0.3316,0.3413,0.3517,0.3627,0.3744,0.3868,0.4002,0.4145,0.4298,0.4463,0.4642,0.4835,0.5046,0.5275,0.5526,0.5802,0.6108,0.6447,0.6826,0.7253,0.7737,0.8289,0.8927,0.9671,1.0550,1.1605,1.2894,1.4506,1.6579,1.9342,2.3210,2.9012,3.8683,5.8025,11.6050};
//double beta[5] = {0.5,1,2,10,50};
//double beta[4] = {0.2321,0.5802,1.1605,5.8025}; // corresponds to T = {50,20,10,2} to compare with Rasmus's results
//double beta[1] = {10};
double beta[285];
double kB = 8.617e-2;    // meV/K
double uB = 5.788838e-2; // meV/T
double g = 2.0;

// Declare material variables
double Jbc_AA, Jb_AA, Jc_AA, Jab_AA, Jac_AA, D14_AA, D_A[3];
double Jbc_BB, Jb_BB, Jc_BB, Jab_BB, Jac_BB, D14_BB, D_B[3];
double Jbc_AB, Jb_AB, Jc_AB, Jab_AB, Jac_AB, D14_AB;
double SA, SB;
double field[3];
double doping;
double aa, bb, cc;
char inputpar[5][200] = {"LiNiFePO4_paraSimple30.ini","LiNiFePO4_paraSimple30.ini","LiNiFePO4_paraSimple30.ini","LiNiFePO4_paraSimple30.ini","LiNiFePO4_paraSimple30.ini"}; // File with input parameters for exchange, anisotropy, doping and so on
char inputconfig[200];// = "sim_startconfig_L4_oneFe.dat"; // Input file for simulated annealing or if random config is not generated
char endconfigout[200]; // Output file for resulting lattice
char magfilename[200]; // Output file for average magnetization along x, y and z and total energy for each MCS
char magfilesample[200]; // Output file for sample magnetizattion along x, y and z and total energy for calculating specific heat and susceptiility
char parameters[200]; // File containing simulation parameters like exchange interactions, anisotropies etc.
int gen = 1; // Generate initial lattice configuration? 1: Yes, 0 (or something else): No
int san = 1; // Simulated annealing? 1: Yes, 0 (or something else): No
int tofile  = 0; // Print magnetization and energy to file? 1: Yes, 0 (or something else): No


// Global variables: initialize ion positions, flavors, spins orientations, total energy and neighbors
double position[4*L*L*L][3];
int flavor[4*L*L*L];
double spin[4*L*L*L][3];
double Etot;
int N_A = 0, N_B = 0;
int NNbc[4*L*L*L][4], NNb[4*L*L*L][2], NNc[4*L*L*L][2], NNab[4*L*L*L][4], NNac[4*L*L*L][4];
double Jbc[4*L*L*L][4], Jb[4*L*L*L][2], Jc[4*L*L*L][2], Jab[4*L*L*L][4], Jac[4*L*L*L][4];
double newspin[3];

// Initialize functions
void readpar(int p);
void genlattice();
void loadlattice();
int getionflavor(double p);
void getspincoor(int m, double S);
void getNNbc();
void getNNb();
void getNNc();
void getNNab();
void getNNac();
double assigninteractions();
double getexenergy(int m);
double getanienergy(int m);
double getdmenergy(int m);
double getzeenergy(int m);
double getmagnetization(int m, int t);
double getenergychange(int m, double Sx, double Sy, double Sz);
void getnewspincoor(double S);



int main(void)
{

  int p;
  for (p = 0; p < sizeof(inputpar)/sizeof(inputpar[0]); p++){

    clock_t begin = clock();
    
    // making folder for results
    char resultdir[100];
    sprintf(resultdir,"results%i",p);
    mkdir(resultdir,0777);

    // getting material constants from file and printing them in new file for later reference
    sprintf(parameters,"%s/simulationparameters.dat",resultdir);
    readpar(p);

    srand (time(NULL)); // resetting random seed, quite important I discovered

    // looping over temperatures
    int n;
    for (n = 0; n < sizeof(beta)/sizeof(beta[0]); n++) {
      // hack to run temperature scan like Rasmus
      double T = 100-0.35*n;
      beta[n] = 1/(kB*T);

      // initialize lattice
      if (san == 1) { // simulated annealing
	if (n == 0) { // highest temperature
	  if (gen == 1) {
	    genlattice();} // generate lattice
	  else {
	    loadlattice();} // feed in lattice
	}
	else {} // remaining temperatures
      }
      else { // quenching or starting from other, known starting point.
	if (gen == 1) {
	  genlattice();}
	else {
	  loadlattice();}
      }


      // assign nearest neighbors, assign interactions and calculate energy of start configuration
      getNNbc();
      getNNb();
      getNNc();
      getNNab();
      getNNac();
      assigninteractions();

      // calculating total energy for starting configuration
      int k;
      Etot = 0;
      for (k = 0; k < 4*L*L*L; k++) {
	Etot = Etot + getexenergy(k) + getanienergy(k) + getdmenergy(k) +  getzeenergy(k);
      }

      /* for (k = 0; k < 4*L*L*L; k++){ */
      /*   printf("%3i:%5i%5i%5i%5i\n",k,NNbc[k][0],NNbc[k][1],NNbc[k][2],NNbc[k][3]); */
      /* } */
      /* printf("\n"); */

            // print initial spin configuration and total energy to screen
      /* int c; */
      /* for (c = 0; c < 4*L*L*L; c++){ */
      /*   printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",spin[c][0],spin[c][1],spin[c][2]);} */
      /* printf("Total energy: %7.4f\n\n",Etot); */

      
      // variables for moving spins around
      int nmoves = 100000, amoves = 0, moves = 0; // number of MCSs to complete simulation, number of accepted moves, number of attempted moves
      double S;
      int N = 4*L*L*L, MCS = 0;
      double A[3] = {0.0,0.0,0.0}, G[3] = {0.0,0.0,0.0}, C[3] = {0.0,0.0,0.0}, F[3] = {0.0,0.0,0.0}, Etotcheck, Ebin = 0;
      int nbin = 10000; // number of bins
      int bin = nmoves/nbin; // moves per bin
      int m;
      
      

      if (n == 0){ // printing some information to screen and files at start of the simulation
	
	printf("Width of simulation cube: %d unit cells.\n", L);
	printf("Number of unit cells: %d \n", L*L*L);
	printf("Number of A ions: %d \n", N_A);
	printf("Number of B ions: %d \n", N_B);
	printf("----------------------------------\n");
	printf("\n");

	sprintf(endconfigout,"%s/sim_Ni%.2f_Fe%.2f_%imoves_L%i_%.4fbeta_start.dat",resultdir,1-doping,doping,nmoves,L,beta[n]);
	FILE *outfile = fopen(endconfigout, "w");
	for (k = 0; k < 4*L*L*L; k++) {
	  fprintf(outfile,"%3i %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f \n",flavor[k],position[k][0],position[k][1],position[k][2],spin[k][0],spin[k][1],spin[k][2]);}
	fclose(outfile);
	
      };

      // print start magnetization and total energy to file for each temperature
      sprintf(magfilesample,"%s/sampling_Ni%.2f_Fe%.2f_%imoves_L%i_%.4fbeta.dat",resultdir,1-doping,doping,nmoves,L,beta[n]);
      FILE *magsample = fopen(magfilesample, "w");
      int c;
      for (c = 0; c < 4; c++){
      	fprintf(magsample,"%20.10f %20.10f %20.10f ",getmagnetization(0,c),getmagnetization(1,c),getmagnetization(2,c));}
      fprintf(magsample,"%20.10f\n",Etot);

      //      if (tofile == 0){
	sprintf(magfilename,"%s/magnetization_Ni%.2f_Fe%.2f_%imoves_L%i_%.4fbeta.dat",resultdir,1-doping,doping,nmoves,L,beta[n]);
	FILE *magfile = fopen(magfilename,"w");
	for (c = 0; c <  3; c++){
	  A[c] = A[c] + getmagnetization(c,0);
	  G[c] = G[c] + getmagnetization(c,1);
	  C[c] = C[c] + getmagnetization(c,2);
	  F[c] = F[c] + getmagnetization(c,3);}
	fprintf(magfile,"%20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f\n",A[0],A[1],A[2],G[0],G[1],G[2],C[0],C[1],C[2],F[0],F[1],F[2],Etot);
	//       }


      // here the actual simulation takes place
      while (MCS < nmoves) {
   
	// choosing random spin to move
	k = rand() % (4*L*L*L);
	//     printf("Ion to be moved: %i\n",k);
		
	// assign spin values from flavor
	if (flavor[k]==1) {S = SB;} // B ions
	else if (flavor[k]==2) {S = SA;} // A ions
	// attempt move ion at random
	double edif;
	getnewspincoor(S);
	edif = getenergychange(k,newspin[0],newspin[1],newspin[2]);
	// proposed new spin configuration
	/* printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",newspin[0],newspin[1],newspin[2]); */
	/* printf("New total energy (change): %7.4f (%7.4f)\n",Etot+edif,edif); */

	//double boltz = exp(-edif*beta[n]);
	//double r = ((double)rand()) / ((double)RAND_MAX);
     
	if (edif < 0) { // then accept: move the spin to its new position and update the total energy
	  for (c = 0; c < 3; c++){spin[k][c] = newspin[c];}
	  Etot = Etot + edif;
	  /* printf("Accepted, dE < 0\n"); */
	  /* printf("New spin configuration:\n"); */
	  /* for (c = 0; c < 4*L*L*L; c++) { */
	  /*   printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",spin[c][0],spin[c][1],spin[c][2]); */
	  /* } */
	  amoves++;
	}
	else if ( exp(-edif*beta[n]) > (double)rand() / (double)RAND_MAX ) { // accept: move the spin to its new position and update the total energy
	  //else if ( boltz > r) {
	  for (c = 0; c < 3; c++){spin[k][c] = newspin[c];}
	  Etot = Etot + edif;
	  /* printf("Accepted, exp(-dE*beta) > p\n"); */
	  /* printf("New spin configuration:\n"); */
	  /* for (c = 0; c < 4*L*L*L; c++) { */
	  /*   printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",spin[c][0],spin[c][1],spin[c][2]); */
	  /* } */
	  amoves++;
	}
	else { // don't accept: do nothing
	  /* printf("Rejected\n"); */
	  /* printf("Keep old spin configuration:\n"); */
	  /* for (c = 0; c < 4*L*L*L; c++) { */
	  /*   printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",spin[c][0],spin[c][1],spin[c][2]); */
	  /* } */
	}
      
	// checking whether energy calculated from the differences is the same as if I calculate it from the configuration
	/* Etotcheck = 0.0; */
	/* for (k = 0; k < 4*L*L*L; k++) { */
	/* 	Etotcheck = Etotcheck + getexenergy(k) + getanienergy(k) + getdmenergy(k) + getzeenergy(k); // calculating total energy */
	/* } */
	/* printf("Energy check: %7.4f (%7.4f)\n\n",Etot,Etotcheck); */


	moves++; // counting number of attempted moves in total in order to calculate the number of MCSs


	if (moves == N) {
	  /* Etotcheck = 0.0; */
	  /* for (k = 0; k < 4*L*L*L; k++) { */
	  /*   Etotcheck = Etotcheck + getexenergy(k) + getanienergy(k) + getdmenergy(k) + getzeenergy(k); // calculating total energy */
	  /* } */
	  if (tofile == 1) { // save to file every MCS
	    for (c = 0; c < 4; c++){
	      fprintf(magsample,"%20.10f %20.10f %20.10f ",getmagnetization(0,c),getmagnetization(1,c),getmagnetization(2,c));}
	    fprintf(magsample,"%20.10f\n",Etot);
	  }
	  else if (tofile == 0) {
	    Ebin = Ebin + Etot;
	    for (c = 0; c < 3; c++){
	      A[c] = A[c] + getmagnetization(c,0);
	      G[c] = G[c] + getmagnetization(c,1);
	      C[c] = C[c] + getmagnetization(c,2);
	      F[c] = F[c] + getmagnetization(c,3);}
	    if ( (MCS % bin) == (bin-1) ){ // save to file only for each bin
	      // save values averaged over a bin of iterations
	      fprintf(magfile,"%20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f\n",A[0]/bin,A[1]/bin,A[2]/bin,G[0]/bin,G[1]/bin,G[2]/bin,C[0]/bin,C[1]/bin,C[2]/bin,F[0]/bin,F[1]/bin,F[2]/bin,Ebin/bin);
	      // also save sample values for computing specific heat and susceptibility
	      for (c = 0; c < 4; c++){
		fprintf(magsample,"%20.10f %20.10f %20.10f ",getmagnetization(0,c),getmagnetization(1,c),getmagnetization(2,c));}
	      fprintf(magsample,"%20.10f\n",Etot);
	      Ebin = 0.0;
	      for (c = 0; c < 3; c++){
		A[c] = 0.0;
		G[c] = 0.0;
		C[c] = 0.0;
		F[c] = 0.0;}
	    }
	  }
	  MCS++;
	  moves = 0; // resetting number of moves to count the next MCS
	}


	// print resulting spin configuration to screen
	/* for (c = 0; c < 4*L*L*L; c++){ */
	/* 	printf("(Sx,Sy,Sz) = (%5.4f %5.4f %5.4f)\n",spin[c][0],spin[c][1],spin[c][2]);} */
	/* printf("Total energy: %7.4f\n\n",Etot); */
	/* printf("\n"); */


      }

      fclose(magfile);
      fclose(magsample);


      printf("Done with simulation at beta = %.4f\n",beta[n]);
      //    printf("Number of moves: %i\n",nmoves);
      printf("Number of accepted moves in total: %i\n",amoves);
      printf("Number of MCS performed: %i\n",MCS);
      printf("Final total energy: %.4f\n\n",Etot);

      // printing end spin configuration to file for checks and plotting
      if (n == (sizeof(beta)/sizeof(beta[0])-1) ){
	sprintf(endconfigout,"%s/sim_Ni%.2f_Fe%.2f_%imoves_L%i_%.4fbeta_end.dat",resultdir,1-doping,doping,nmoves,L,beta[n]);
	FILE *outfile2 = fopen(endconfigout, "w");
	for (k = 0; k < 4*L*L*L; k++) {
	  fprintf(outfile2,"%3i %20.10f %20.10f %20.10f %20.10f %20.10f %20.10f \n",flavor[k],position[k][0],position[k][1],position[k][2],spin[k][0],spin[k][1],spin[k][2]);}
	fclose(outfile2);}


    }



    // calculating runtime and printing to screen
    clock_t end = clock();
    double runtime = (double)(end - begin) / CLOCKS_PER_SEC / 60;
    printf("Runtime for simulations number %i/%lu in batch: %.4fmin\n\n",p+1,sizeof(inputpar)/sizeof(inputpar[0]),runtime);

  }

}

void genlattice(){

  // Building lattice

  // Declearing variables and initializing some of them
  int i, j, l, m, k;
  double S;

  //  FILE *outfile = fopen(initconfigout, "w");

  // filling in 4 ions per unit cell
  k = 0;
  for (m = 0; m < L; m++){
    for ( j = 0; j < L; j++ ){
      for ( l = 0; l < L; l++ ) {
	// filling in ions
	for ( i = 0; i < 4; i++ ){	
	  flavor[k] = (int) getionflavor(doping);
	  if (flavor[k] == 1) {
	    N_B++; S = SB;}
	  else if (flavor[k] == 2) {
	    N_A++; S = SA;}
	  getspincoor(k,S);
	  position[k][0] = (m + dx[i])*aa; // only one layer is initialized until I know what I'm doing
	  position[k][1] = (j + dy[i])*bb;
	  position[k][2] = (l + dz[i])*cc;
	  //	  fprintf(outfile,"%3i %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f \n",flavor[k],position[k][0],position[k][1],position[k][2],spin[k][0],spin[k][1],spin[k][2]);
	  k++;
	}
      }
    }
  }

  //  fclose(outfile);

}


void loadlattice()
{

  FILE*fp;
  int j = 0;
  int dummy1;
  double dummy2[6];

  fp = fopen(inputconfig, "r");

  while ( fscanf(fp, "%d %lf %lf %lf %lf %lf %lf",&dummy1,&dummy2[0],&dummy2[1],&dummy2[2],&dummy2[3],&dummy2[4],&dummy2[5]) == 7 ) {

    flavor[j] = dummy1;
    if (flavor[j] == 1) {N_B ++;} else if (flavor[j] == 2) {N_A ++;};
    position[j][0] = dummy2[0]; position[j][1] = dummy2[1]; position[j][2] = dummy2[2];
    spin[j][0] = dummy2[3]; spin[j][1] = dummy2[4]; spin[j][2] = dummy2[5];
    //printf("%3i %5.2lf %5.2lf %5.2lf %5.2lf %5.2lf %5.2lf \n", flavor[j], position[j][0], position[j][1],position[j][2],spin[j][0],spin[j][1],spin[j][2] );
    
    j++;
    
    
  }
	
  fclose(fp);

}


void readpar(int p)
{
  FILE*fp;
  double tmp;
  fp = fopen(inputpar[p], "r");
  
  printf("----------------------------------\n");
  
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jbc_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jbc_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jbc_AB = (double) tmp;

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jb_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jb_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jb_AB = (double) tmp;

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jc_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jc_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jc_AB = (double) tmp;

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jab_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jab_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jab_AB = (double) tmp;

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jac_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jac_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  Jac_AB = (double) tmp;

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D14_AA = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D14_BB = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D14_AB = (double) tmp;

  printf("Interactions for A-A are read as (Jbc,Jb,Jc,Jab,Jac,D14) = (%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf) \n", Jbc_AA,Jb_AA,Jc_AA,Jab_AA,Jac_AA,D14_AA);
  printf("Interactions for B-B are read as (Jbc,Jb,Jc,Jab,Jac,D14) = (%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf) \n", Jbc_BB,Jb_BB,Jc_BB,Jab_BB,Jac_BB,D14_BB);
  printf("Interactions for A-B are read as (Jbc,Jb,Jc,Jab,Jac,D14) = (%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf,%5.2lf) \n", Jbc_AB,Jb_AB,Jc_AB,Jab_AB,Jac_AB,D14_AB);

  printf("----------------------------------\n");

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_A[0] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_A[1] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_A[2] = (double) tmp;
  printf("Single-ion anisotropy of A is read as (Da,Db,Dc) = (%.2lf,%.2lf,%.2lf) \n", D_A[0],D_A[1],D_A[2]);

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_B[0] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_B[1] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  D_B[2] = (double) tmp;
  printf("Single-ion anisotropy of B is read as (Da,Db,Dc) = (%.2lf,%.2lf,%.2lf) \n", D_B[0],D_B[1],D_B[2]);

  printf("----------------------------------\n");

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  SA = (double) tmp;
  printf("The spin of ion A is read as: %.2lf \n", SA);
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  SB = (double) tmp;
  printf("The spin of ion B is read as: %.2lf \n", SB);

  printf("----------------------------------\n");

  fscanf(fp, "%*[^\n]\n%lf\n", &tmp);
  field[0] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n", &tmp);
  field[1] = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n", &tmp);
  field[2] = (double) tmp;
  printf("Magnetic field is read as (Ha,Hb,Hc) = (%.2lf T,%.2lf T,%.2lf T)\n", field[0],field[1],field[2]);

  fscanf(fp, "%*[^\n]\n%lf\n", &tmp);
  doping = (double) tmp;
  printf("Doping level is read as %.2lf\n", doping);

  printf("----------------------------------\n");

  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  aa = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  bb = (double) tmp;
  fscanf(fp, "%*[^\n]\n%lf\n",&tmp);
  cc = (double) tmp;
  printf("Lattice parameters are read as (a,b,c) = (%.3lf,%.3lf,%.3lf) \n", aa,bb,cc);

  printf("----------------------------------\n");
	
  fclose(fp);

  FILE *parafile = fopen(parameters, "w");

  fprintf(parafile,"Exchange interactions:\n");
  fprintf(parafile,"----------------------\n");
  fprintf(parafile,"Jbc_AA = %7.3lf, Jb_AA = %7.3lf, Jc_AA = %7.3lf, Jab_AA = %7.3lf, Jac_AA = %7.3lf\n",Jbc_AA,Jb_AA,Jc_AA,Jab_AA,Jac_AA);
  fprintf(parafile,"Jbc_BB = %7.3lf, Jb_BB = %7.3lf, Jc_BB = %7.3lf, Jab_BB = %7.3lf, Jac_BB = %7.3lf\n",Jbc_BB,Jb_BB,Jc_BB,Jab_BB,Jac_BB);
  fprintf(parafile,"Jbc_AB = %7.3lf, Jb_AB = %7.3lf, Jc_AB = %7.3lf, Jab_AB = %7.3lf, Jac_AB = %7.3lf\n",Jbc_AB,Jb_AB,Jc_AB,Jab_AB,Jac_AB);
  fprintf(parafile,"\nDzyaloshinskii-Moriya interactions:\n");
  fprintf(parafile,"-----------------------------------\n");
  fprintf(parafile,"D14_AA = %7.3lf, D14_BB = %7.3lf, D14_AB = %7.3lf\n",D14_AA,D14_BB,D14_AB);
  fprintf(parafile,"\nSingle-ion anisotropies:\n");
  fprintf(parafile,"------------------------\n");
  fprintf(parafile,"Dx_A = %7.3lf, Dy_A = %7.3lf, Dz_A = %7.3lf\n",D_A[0],D_A[1],D_A[2]);
  fprintf(parafile,"Dx_B = %7.3lf, Dy_B = %7.3lf, Dz_B = %7.3lf\n",D_B[0],D_B[1],D_B[2]);
  fprintf(parafile,"\nIons, spin and magnetic field:\n");
  fprintf(parafile,"------------------------------\n");
  fprintf(parafile,"x = %7.3lf\n",doping);
  fprintf(parafile,"SA = %5.2lf, SB = %5.2lf\n",SA,SB);
  fprintf(parafile,"uH = (%5.2lf T, %5.2lf T, %5.2lf T)\n",field[0],field[1],field[2]);
  fprintf(parafile,"\nLattice parameters:\n");
  fprintf(parafile,"--------------------\n");
  fprintf(parafile,"a = %7.3lf, b = %7.3lf, c = %7.3lf\n",aa,bb,cc);

  fclose(parafile);

  return;

}

int getionflavor(double p) {
  double r = (double)rand() / (double)RAND_MAX;
  if (r<=p) { return 1;} // B ion
  else      { return 2;} // A ions
}

void getspincoor(int m, double S) {
  double v1, v2, v;
  v1 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
  v2 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
  v = v1*v1 + v2*v2;
  while (v >= 1){
    v1 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
    v2 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
    v = v1*v1 + v2*v2;
  }
  spin[m][0] = S*2*v1*sqrt(1-v);
  spin[m][1] = S*2*v2*sqrt(1-v);
  spin[m][2] = S*(1-2*v);
}

void getnewspincoor(double S) {
  double v1, v2, v;
  v1 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
  v2 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
  v = v1*v1 + v2*v2;
  while (v >= 1){
    v1 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
    v2 = 2.0*(double)rand() / (double)RAND_MAX - 1.0;
    v = v1*v1 + v2*v2;
  }
  newspin[0] = S*2*v1*sqrt(1-v);
  newspin[1] = S*2*v2*sqrt(1-v);
  newspin[2] = S*(1-2*v);
}

void getNNbc() {

  // assigning indices of nearest neighbors in the (b,c)-plane
  // finding out whether atom n belongs to the bulk, corners or any of the sides

  int l, m, n, k;
  int corner[4*L];
  int left[2*(L-1)*L], bottom[2*(L-1)*L], top[2*(L-1)*L], right[2*(L-1)*L];

  k = 0;
  for (l = 0; l < L; l++) for (m = 1; m < 2*(L-1)+1; m++) {
      left[k] = 2+2*m + l*4*L*L; 
      k++; }
  k = 0;
  for (l = 0; l < L; l++) for (m = 0; m < L-1; m++) {
      bottom[k] = 3 + m*4*L + l*4*L*L;
      top[k] = (m+1)*4*L-3 + l*4*L*L;
      right[k] = -1 - 4*m + 4*L*L*L - l*4*L*L;
      k++;
      bottom[k] = 2 + (m+1)*4*L + l*4*L*L;
      top[k] = (m+2)*4*L + l*4*L*L;
      right[k] = -7 - 4*m + 4*L*L*L - l*4*L*L; 
      k++; }
  k = 0;
  for (l = 0; l < L; l++) {
    corner[k] = 2 + l*4*L*L; k++;
    corner[k] = 4*L + l*4*L*L; k++;
    corner[k] = 4*L*L-4*L+3 + l*4*L*L; k++;
    corner[k] = 4*L*L-3 + l*4*L*L; k++;
  }

  for (n = 1; n < 4*L*L*L+1; n++){
    // filling in as if all ions belong to the bulk first
    if (n % 4 == 1)      { NNbc[n-1][0] = n+1; NNbc[n-1][1] = n+5; NNbc[n-1][2] = n+(4*L+1); NNbc[n-1][3] = n+(4*L+5);}
    else if (n % 4 == 2) { NNbc[n-1][0] = n-(4*L+5); NNbc[n-1][1] = n-(4*L+1); NNbc[n-1][2] = n-5; NNbc[n-1][3] = n-1;}
    else if (n % 4 == 3) { NNbc[n-1][0] = n-3; NNbc[n-1][1] = n+1; NNbc[n-1][2] = n+(4*L-3); NNbc[n-1][3] = n+(4*L+1);}
    else if (n % 4 == 0) { NNbc[n-1][0] = n-(4*L+1); NNbc[n-1][1] = n-(4*L-3); NNbc[n-1][2] = n-1; NNbc[n-1][3] = n+3;}
    // fillling in on corners and sides
    for (l = 0; l < 4*L; l++) {
      if (n == corner[l]) { // checking for corners first
    	if (n % 4 == 1)      { NNbc[n-1][1] = NNbc[n-1][1]-4*L; NNbc[n-1][2] = NNbc[n-1][2]-4*L*L; NNbc[n-1][3] = NNbc[n-1][3]-4*L*L-4*L;}
    	else if (n % 4 == 2) { NNbc[n-1][0] = NNbc[n-1][0]+4*L*L+4*L; NNbc[n-1][1] = NNbc[n-1][1]+4*L*L; NNbc[n-1][2] = NNbc[n-1][2]+4*L;}
	else if (n % 4 == 3) { NNbc[n-1][0] = NNbc[n-1][0]-4*L*(L-2); NNbc[n-1][3] = NNbc[n-1][3]-4*L*L;}
	else if (n % 4 == 0) { NNbc[n-1][0] = NNbc[n-1][0]+4*L*L; NNbc[n-1][3] = NNbc[n-1][3]+4*L*(L-2);}
      }
    }
    for (l = 0; l < 2*(L-1)*L; l++) { // checking sides
      if (n == left[l])        {NNbc[n-1][0] = NNbc[n-1][0]+4*L*L; NNbc[n-1][1] = NNbc[n-1][1]+4*L*L;}
      else if (n == right[l]) {NNbc[n-1][2] = NNbc[n-1][2]-4*L*L; NNbc[n-1][3] = NNbc[n-1][3] -4*L*L;}
      else if (n == bottom[l])    {NNbc[n-1][0] = NNbc[n-1][0]+8*L;}
      else if (n == top[l])  {NNbc[n-1][3] = NNbc[n-1][3]-8*L;}
    }
    // subtracting one such that indices make sense for C
    for (l = 0; l < 4; l++) { NNbc[n-1][l] = NNbc[n-1][l]-1;}
    //    printf("%d: %d %d %d %d\n",n-1,NNbc[n-1][0],NNbc[n-1][1],NNbc[n-1][2],NNbc[n-1][3]);
  }

}

void getNNb() {

  // assigning nearest neighbors along b
  // this is somewhat easier than the next nearest neighbors in the (b,c)-plane
  int n, l, k, i;
  int left[4*L*L], right[4*L*L];

  if (L == 1){ // special case for L = 1
    for (n = 0; n < 4*L*L*L; n++){
      NNb[n][0] = n; NNb[n][1] = n;}
  }
  else {
    k = 0;
    for (l = 0; l < L; l++) for (i = 1; i < 4*L+1; i++) {
	left[k] = i + l*4*L*L; 
	right[k] = 4*L*L-4*L+i + l*4*L*L; 
	k++;
      }

    for (n = 1; n < 4*L*L*L+1; n++) {
      // filling in all ions as if they belong to the bulk first
      NNb[n-1][0] = n-4*L; NNb[n-1][1] = n+4*L;
      for (l = 0; l < 4*L*L; l++) {
	if (n == left[l]) {
	  NNb[n-1][0] = NNb[n-1][0]+4*L*L;}
	else if (n == right[l]) {
	  NNb[n-1][1] = NNb[n-1][1]-4*L*L;}
      }
      // subtracting one such that indices make sense for C
      for (l = 0; l < 2; l++) { NNb[n-1][l] = NNb[n-1][l]-1;}
    }
  }
  
}

void getNNc() {

  // assigning nearest neighbors along c
  int n, l, k, i, m;
  int bottom[4*L*L], top[4*L*L];

  if (L == 1){ // special case for L = 1
    for (n = 0; n < 4*L*L*L; n++){
      NNc[n][0] = n; NNc[n][1] = n;}
  }
  else {
    k = 0;
    for (l = 0; l < L; l++) for (m = 0; m < L; m++) for (i = 1; i < 5; i++) {
	  bottom[k] = i + m*4*L + l*4*L*L;
	  top[k] = 4*(L-1)+i + m*4*L + l*4*L*L;
	  k++;
	}

    for (n = 1; n < 4*L*L*L+1; n++) {
      // filling in all ions as if they belong to the bulk first
      NNc[n-1][0] = n-4; NNc[n-1][1] = n+4;
      for (l = 0; l < 4*L*L; l++) {
	if (n == bottom[l]) {
	  NNc[n-1][0] = NNc[n-1][0]+4*L;}
	else if (n == top[l]) {
	  NNc[n-1][1] = NNc[n-1][1]-4*L;}
      }
      // subtracting one such that indices make sense for C
      for (l = 0; l < 2; l++) { NNc[n-1][l] = NNc[n-1][l]-1;}
    }
  }
  
}

void getNNab() {

  // assigning nearest neighbors in the (a,b)-plane
  int m, k, l, n;
  int corner[4*L], left[2*L*(L-1)], bottom[2*L*(L-1)], top[2*L*(L-1)], right[2*L*(L-1)];

  k = 0;
  for (m = 0; m < L; m++) {
    corner[k] = 2+4*m; k++;
    corner[k] = 4*L*(L-1)+1+4*m; k++;
    corner[k] = 4*L*L*L-4*L+3+4*m; k++;
    corner[k] = 4*L*L*L-4*L*L+4+4*m; k++;
  }
  k = 0;
  for (m = 0; m < L*(L-1); m++) {
    left[k] = 1+4*m; 
    right[k] = 4*L*L*L-4*L*L+4*(m+1)-1;
    k++;
    left[k] = 2+4*L+4*m;
    right[k] = 4*L*L*L-4*L*(L-1)+4*(m+1);
    k++;
  }
  k = 0;
  for (l = 0; l < L-1; l++) for (m = 0; m < L; m++) {
      bottom[k] = 4+4*m + l*4*L*L;
      top[k] = 4*L*L-4*L+3+4*m + l*4*L*L;
      k++;
      bottom[k] = 2+4*m+(l+1)*4*L*L;
      top[k] = 4*L*L-4*L+1+4*m + (l+1)*4*L*L;
      k++;
  }

  for (n = 1; n < 4*L*L*L+1; n++) {
    // filling in all ions as if they belong to the bulk first
    if (n % 4 == 1) {
      NNab[n-1][0] = n-(4*L*L-3); NNab[n-1][1] = n-(4*L*(L-1)-3); NNab[n-1][2] = n+3; NNab[n-1][3] = n+(4*L+3);}
    else if (n % 4 == 2) {
      NNab[n-1][0] = n-(4*L*L+4*L-1); NNab[n-1][1] = n-(4*L*L-1); NNab[n-1][2] = n-(4*L-1); NNab[n-1][3] = n+1;}
    else if (n % 4 == 3) {
      NNab[n-1][0] = n-1; NNab[n-1][1] = n+(4*L-1); NNab[n-1][2] = n+(4*L*L-1); NNab[n-1][3] = n+(4*L*L+4*L-1);}
    else if (n % 4 == 0) {
      NNab[n-1][0] = n-(4*L+3); NNab[n-1][1] = n-3; NNab[n-1][2] = n+(4*L*(L-1)-3); NNab[n-1][3] = n+(4*L*L-3);}
    
    for (l = 0; l < 4*L; l++) {
      if (n == corner[l]) { // filling neighbors for the ions on the corners
	if (n % 4 == 1) {
	  NNab[n-1][0] = NNab[n-1][0]+4*L*L*L; NNab[n-1][3] = NNab[n-1][3]+4*L*L*(L-2);}
	else if (n % 4 == 2) {
	  NNab[n-1][0] = NNab[n-1][0]+4*L*L*(L+1); NNab[n-1][1] = NNab[n-1][1]+4*L*L*L; NNab[n-1][2] = NNab[n-1][2]+4*L*L;}
	else if (n % 4 == 3) {
	  NNab[n-1][1] = NNab[n-1][1]-4*L*L; NNab[n-1][2] = NNab[n-1][2]-4*L*L*L; NNab[n-1][3] = NNab[n-1][3]-4*L*L*(L+1);}
	else if (n % 4 == 0) {
	  NNab[n-1][0] = NNab[n-1][0]-4*L*L*(L-2); NNab[n-1][3] = NNab[n-1][3]-4*L*L*L;}
      }
    }
    for (l = 0; l < 2*L*(L-1); l++) {
      if (n == left[l]) { // filling in neighbors for left side ions
	NNab[n-1][0] = NNab[n-1][0]+4*L*L*L; NNab[n-1][1] = NNab[n-1][1]+4*L*L*L;}
      else if (n == bottom[l]){ // filling in neighbors for the bottom ions
	NNab[n-1][0] = NNab[n-1][0]+8*L*L;}
      else if (n == top[l]) { // filling in neighbors for the top ions
	NNab[n-1][3] = NNab[n-1][3]-8*L*L;}
      else if (n == right[l]) { // filling in neighbors for the right side ions
	NNab[n-1][2] = NNab[n-1][2]-4*L*L*L; NNab[n-1][3] = NNab[n-1][3]-4*L*L*L;}
    }
    // subtracting one such that indices make sense for C
    for (l = 0; l < 4; l++) { NNab[n-1][l] = NNab[n-1][l]-1;}
  }


}

void getNNac() {

  int l, k, m, n;
  int corner[4*L], left[2*L*(L-1)], bottom[2*L*(L-1)], top[2*L*(L-1)], right[2*L*(L-1)];

  k = 0;
  for (l = 0; l < L; l++) {
    corner[k] = 2 + l*4*L; k++;
    corner[k] = 4*L*L*L - l*4*L; k++;
    corner[k] = -3 + (l+1)*4*L; k++;
    corner[k] = 4*L*L*L + 3 - (l+1)*4*L; k++;
  }
  k = 0;
  for (l = 0; l < L; l++) for (m = 0; m < L-1; m++) {
      left[k] = 1 + 4*m + 4*l*L; 
      bottom[k] = 3 + 4*m*L*L + l*4*L;
      top[k] = 4*L-3 + 4*(m+1)*L*L + l*4*L;
      right[k] = 4*L*L*L - 4*(m+1) - l*4*L; 
      k++;
      left[k] = 6 + 4*m + 4*l*L; 
      bottom[k] = 2 + 4*(m+1)*L*L + l*4*L; 
      top[k] = 4*L + 4*m*L*L + l*4*L;
      right[k] = 4*L*L*L-1 - 4*m - l*4*L; 
      k++;
  }

  for (n = 1; n < 4*L*L*L+1; n++) {
    // filling in all atoms as if they are bulk ones at first
    if (n % 4 == 1) {
      NNac[n-1][0] = n-(4*L*L-2); NNac[n-1][1] = n-(4*L*L-6); NNac[n-1][2] = n+2; NNac[n-1][3] = n+6;}
    else if (n % 4 == 2) {
      NNac[n-1][0] = n-(4*L*L-2); NNac[n-1][1] = n-(4*L*L+2); NNac[n-1][2] = n+2; NNac[n-1][3] = n-2;}
    else if (n % 4 == 3) {
      NNac[n-1][0] = n-6; NNac[n-1][1] = n-2; NNac[n-1][2] = n+(4*L*L-6); NNac[n-1][3] = n+(4*L*L-2);}
    else if (n % 4 == 0){
      NNac[n-1][0] = n+2; NNac[n-1][1] = n-2; NNac[n-1][2] = n+(4*L*L+2); NNac[n-1][3] = n+(4*L*L-2);}

    for (l = 0; l < 4*L; l++) {
      if (n == corner[l]) { // filling in neighbors for corner atoms
    	if (n % 4 == 1) {
    	  NNac[n-1][0] = NNac[n-1][0]+4*L*L*L; NNac[n-1][1] = NNac[n-1][1]+(4*L*L*L-4*L); NNac[n-1][3] = NNac[n-1][3]-4*L;}
	else if (n % 4 == 2) {
	  NNac[n-1][0] = NNac[n-1][0]+4*L*L*L; NNac[n-1][1] = NNac[n-1][1]+(4*L*L*L+4*L); NNac[n-1][3] = NNac[n-1][3]+4*L;}
    	else if (n % 4 == 3) {
    	  NNac[n-1][0] = NNac[n-1][0]-(4*L*L*L-4*L*L-4*L); NNac[n-1][2] = NNac[n-1][2]-4*L*(L-1); NNac[n-1][3] = NNac[n-1][3]-4*L*L*L;}
	else if (n % 4 == 0) {
	  NNac[n-1][0] = NNac[n-1][0]-4*L; NNac[n-1][2] = NNac[n-1][2]-(4*L*L*L+4*L); NNac[n-1][3] = NNac[n-1][3]-4*L*L*L;}
      }
    }
    
    for (l = 0; l < 2*L*(L-1); l++) {
      if (n == left[l]) { // left ones
    	NNac[n-1][0] = NNac[n-1][0]+4*L*L*L; NNac[n-1][1] = NNac[n-1][1]+4*L*L*L;}
      else if (n == right[l]) { // right ones
	NNac[n-1][2] = NNac[n-1][2]-4*L*L*L; NNac[n-1][3] = NNac[n-1][3]-4*L*L*L;
      }
      else if (n == bottom[l]) {
	if (n % 4 == 2) {
	  NNac[n-1][1] = NNac[n-1][1]+4*L; NNac[n-1][3] = NNac[n-1][3]+4*L;}
	else if (n % 4 == 3) {
	  NNac[n-1][0] = NNac[n-1][0]+4*L; NNac[n-1][2] = NNac[n-1][2]+4*L;}
      }
      else if (n == top[l]) {
	if (n % 4 == 1) {
	  NNac[n-1][1] = NNac[n-1][1]-4*L; NNac[n-1][3] = NNac[n-1][3]-4*L;}
	else if (n % 4 == 0){
	  NNac[n-1][0] = NNac[n-1][0]-4*L; NNac[n-1][2] = NNac[n-1][2]-4*L;}
      }
    }
    // subtracting one such that indices make sense for C
    for (l = 0; l < 4; l++) { NNac[n-1][l] = NNac[n-1][l]-1;}
  }


}

double assigninteractions() {

  int c, k;
  
  for (k = 0; k < 4*L*L*L; k++){

    // assign interactions in the bc plane
    for (c = 0; c < 4; c++){
      if (flavor[k] != flavor[(NNbc[k][c])]){Jbc[k][c] = Jbc_AB;}
      else {
	if (flavor[k] == 1) {Jbc[k][c] = Jbc_BB;}
	else if (flavor[k] == 2) {Jbc[k][c] = Jbc_AA;}
      }
    }
    // assign interactions in the ab-plane
    for (c = 0; c < 4; c++){
      if (flavor[k] != flavor[(NNab[k][c])]){Jab[k][c] = Jab_AB;}
      else {
	if (flavor[k] == 1) {Jab[k][c] = Jab_BB;}
	else if (flavor[k] == 2) {Jab[k][c] = Jab_AA;}
      }
    }
    // assign interactions in the ac-plane
    for (c = 0; c < 4; c++){
      if (flavor[k] != flavor[(NNac[k][c])]){Jac[k][c] = Jac_AB;}
      else {
	if (flavor[k] == 1) {Jac[k][c] = Jac_BB;}
	else if (flavor[k] == 2) {Jac[k][c] = Jac_AA;}
      }
    }
    // assign interactions along b
    for (c = 0; c < 2; c++){
      if (flavor[k] != flavor[(NNb[k][c])]){Jb[k][c] = Jb_AB;}
      else {
	if (flavor[k] == 1) {Jb[k][c] = Jb_BB;}
	else if (flavor[k] == 2) {Jb[k][c] = Jb_AA;}
      }
    }
    // assign interactions along c
    for (c = 0; c < 2; c++){
      if (flavor[k] != flavor[(NNc[k][c])]){Jc[k][c] = Jc_AB;}
      else {
	if (flavor[k] == 1) {Jc[k][c] = Jc_BB;}
	else if (flavor[k] == 2) {Jc[k][c] = Jc_AA;}
      }
    }

  }

}

double getexenergy(int m) {

  int c;
  double ex = 0.0;

  for (c = 0; c < 4; c++){
    ex = ex + Jbc[m][c]*(spin[(NNbc[m][c])][0]*spin[m][0] + spin[(NNbc[m][c])][1]*spin[m][1] + spin[(NNbc[m][c])][2]*spin[m][2]);
    ex = ex + Jab[m][c]*(spin[(NNab[m][c])][0]*spin[m][0] + spin[(NNab[m][c])][1]*spin[m][1] + spin[(NNab[m][c])][2]*spin[m][2]);
    ex = ex + Jac[m][c]*(spin[(NNac[m][c])][0]*spin[m][0] + spin[(NNac[m][c])][1]*spin[m][1] + spin[(NNac[m][c])][2]*spin[m][2]);
  }
  for (c = 0; c < 2; c++) {
    ex = ex + Jb[m][c]*(spin[(NNb[m][c])][0]*spin[m][0] + spin[(NNb[m][c])][1]*spin[m][1] + spin[(NNb[m][c])][2]*spin[m][2]);
    ex = ex + Jc[m][c]*(spin[(NNc[m][c])][0]*spin[m][0] + spin[(NNc[m][c])][1]*spin[m][1] + spin[(NNc[m][c])][2]*spin[m][2]);
  }
  ex = 0.5*ex; // taking into account double counting when summing over all spins

  return ex;

}

double getanienergy(int m) {

  double ani;
	
  if (flavor[m] == 1) {
    ani = D_B[0]*spin[m][0]*spin[m][0] + D_B[1]*spin[m][1]*spin[m][1] + D_B[2]*spin[m][2]*spin[m][2];}
  if (flavor[m] == 2) {
    ani = D_A[0]*spin[m][0]*spin[m][0] + D_A[1]*spin[m][1]*spin[m][1] + D_A[2]*spin[m][2]*spin[m][2];}

  return ani;
}

double getdmenergy(int m) {

  double D14, dm = 0.0;
  int c, cmax;

  if (L == 1){ // special case for L = 1 to make sure each bond is only counted once
    cmax = 1;}
  else{
    cmax = 4;}

  for (c = 0; c < cmax; c++){
    if ( (m % 4 == 0) | (m % 4 == 2) ){ // only taking sublattice 0 and 2 into account to avoid double counting
      if (flavor[m] != flavor[(NNbc[m][c])]){
	D14 = D14_AB;}
      else if (flavor[m] == 1){
	D14 = D14_BB;}
      else if (flavor[m] == 2){
	D14 = D14_AA;}

      if ( m % 4 == 2){ D14 = -D14; } // swap sign for sublattice 2
      
      dm = dm + D14*( spin[m][2]*spin[(NNbc[m][c])][0] - spin[m][0]*spin[(NNbc[m][c])][2] );
    }
    
  }

  return dm;

}


double getzeenergy(int m){

  return (double) -g*uB*( spin[m][0]*field[0] + spin[m][1]*field[1] + spin[m][2]*field[2] );

}

double getmagnetization(int m, int t){

  int k;
  double M = 0;
  double T[4][4] = { {1.0,1.0,-1.0,-1.0},
		     {-1.0,1.0,1.0,-1.0},
		     {-1.0,1.0,-1.0,1.0},
		     {1.0,1.0,1.0,1.0} };

  for (k = 0; k < 4*L*L*L; k=k+4){

    M = M + spin[k][m]*T[t][0] + spin[k+1][m]*T[t][1] + spin[k+2][m]*T[t][2] + spin[k+3][m]*T[t][3];

  }

  return M;

}


double getenergychange(int m, double Sx, double Sy, double Sz){

  double old = 0.0, new = 0.0, edif, D14;

  int c, cmax;

  // calculating exchange energies
  for (c = 0; c < 4; c++){
    old = old + Jbc[m][c]*(spin[(NNbc[m][c])][0]*spin[m][0] + spin[(NNbc[m][c])][1]*spin[m][1] + spin[(NNbc[m][c])][2]*spin[m][2]);
    old = old + Jab[m][c]*(spin[(NNab[m][c])][0]*spin[m][0] + spin[(NNab[m][c])][1]*spin[m][1] + spin[(NNab[m][c])][2]*spin[m][2]);
    old = old + Jac[m][c]*(spin[(NNac[m][c])][0]*spin[m][0] + spin[(NNac[m][c])][1]*spin[m][1] + spin[(NNac[m][c])][2]*spin[m][2]);

    new = new + Jbc[m][c]*(spin[(NNbc[m][c])][0]*Sx + spin[(NNbc[m][c])][1]*Sy + spin[(NNbc[m][c])][2]*Sz);
    new = new + Jab[m][c]*(spin[(NNab[m][c])][0]*Sx + spin[(NNab[m][c])][1]*Sy + spin[(NNab[m][c])][2]*Sz);
    new = new + Jac[m][c]*(spin[(NNac[m][c])][0]*Sx + spin[(NNac[m][c])][1]*Sy + spin[(NNac[m][c])][2]*Sz);
  }
  if (L > 1) { // for L = 1, the exchange along b and c remains constant and I am only interested in the energy change here
    for (c = 0; c < 2; c++) {
      old = old + Jb[m][c]*(spin[(NNb[m][c])][0]*spin[m][0] + spin[(NNb[m][c])][1]*spin[m][1] + spin[(NNb[m][c])][2]*spin[m][2]);
      old = old + Jc[m][c]*(spin[(NNc[m][c])][0]*spin[m][0] + spin[(NNc[m][c])][1]*spin[m][1] + spin[(NNc[m][c])][2]*spin[m][2]);
      
      new = new + Jb[m][c]*(spin[(NNb[m][c])][0]*Sx + spin[(NNb[m][c])][1]*Sy + spin[(NNb[m][c])][2]*Sz);
      new = new + Jc[m][c]*(spin[(NNc[m][c])][0]*Sx + spin[(NNc[m][c])][1]*Sy + spin[(NNc[m][c])][2]*Sz);
    }
  }

  // adding anisotropy energies
  if (flavor[m] == 1) {
    old = old +  D_B[0]*spin[m][0]*spin[m][0] + D_B[1]*spin[m][1]*spin[m][1] + D_B[2]*spin[m][2]*spin[m][2];

    new = new +  D_B[0]*Sx*Sx + D_B[1]*Sy*Sy + D_B[2]*Sz*Sz;}
  if (flavor[m] == 2) {
    old = old +  D_A[0]*spin[m][0]*spin[m][0] + D_A[1]*spin[m][1]*spin[m][1] + D_A[2]*spin[m][2]*spin[m][2];

    new = new +  D_A[0]*Sx*Sx + D_A[1]*Sy*Sy + D_A[2]*Sz*Sz;}

  // adding zeeman energy
  old = old - g*uB*( spin[m][0]*field[0] + spin[m][1]*field[1] + spin[m][2]*field[2] );
  new = new - g*uB*( Sx*field[0] + Sy*field[1] + Sz*field[2] );
  
  // also adding Dzyalonshinkii-Moriya interaction
  if (L == 1){ // special case for L = 1 to make sure each bond is only counted once
    cmax = 1;}
  else{
    cmax = 4;}
  for (c = 0; c < cmax; c++){ // here we need to consider each ion without worrying about which sublattice it belongs to
    if (flavor[m] != flavor[(NNbc[m][c])]){
      D14 = D14_AB;}
    else if (flavor[m] == 1){
      D14 = D14_BB;}
    else if (flavor[m] == 2){
      D14 = D14_AA;}
    
    if ( (m % 4 == 2) | (m % 4 == 3) ){D14 = -D14;} // change sign if the spin belongs to sublattice 2 or 3
    if ( (m % 4 == 1) | (m % 4 == 3) ){D14 = -D14;} // change sign again/also if the spin belongs to sublattice 1 or 3 (the ones that aren't taken into account in the total energy calculation)

    old = old + D14*( spin[m][2]*spin[(NNbc[m][c])][0] - spin[m][0]*spin[(NNbc[m][c])][2] );

    new = new + D14*( Sz*spin[(NNbc[m][c])][0] - Sx*spin[(NNbc[m][c])][2] );
  }

  edif = new - old;

  return edif;

}
