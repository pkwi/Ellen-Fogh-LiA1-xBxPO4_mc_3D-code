# Ellen Fogh's `LiA1-xBxPO4_mc_3D` c-code
* Please consult [LICENSE](LICENSE) for copyright information.
* For a more in-depth discussion of method and code see [Simulations_overview.pdf](Simulations_overview.pdf)
* Please consult Ellens PhD thesis for supportive scientific information:
https://backend.orbit.dtu.dk/ws/portalfiles/portal/163074073/magneticAndMagnetoelectricPropertiesOfLithiumOrthophosphates_PhDthesis_November2018_EllenFogh.pdf 

## Use e.g. thinlinc / GBAR
* Option 1: (Preferred) Download a client via
  https://www.cendio.com/thinlinc/download, configure it to connect to
  `thinlinc.gbar.dtu.dk`
* Option 2: Use the web-interface via https://thinlinc.gbar.dtu.dk

## Get the code (this repo)
`git clone https://gitlab.gbar.dtu.dk/pkwi/Ellen-Fogh-LiA1-xBxPO4_mc_3D-code`

## Included files

### C-code
The main program is enclosed in the form of a single c-code `LiA1-xBxPO4_mc_3D_batch.c`

### Inputfile example
An input file is given in the form of an `ini` file, `LiNiFePO4_paraSimple30.ini`

## Configuration
1. Create a reasonably named subfolder for a new run
2. Copy c-code and ini file to the subfolder
3. Edit / rename ini file according to the given run 
4. Edit the c-code in the `// Declare material variables` section to indicate which `ini`  file is to be used
5. Optionally set other parameters

## Compilation
Run your edited c-compiler with reasonable `CFLAGS`, e.g.
`gcc -g -O2 -DNDEBUG -lm LiA1-xBxPO4_mc_3D_batch.c -o myrun.out`

(This will enable debugging-symbols, optimise to `O2` level, link with the math library and generate a binary called `myrun.out`)

## Run
Execute the binary generated `./myrun.out`

## Parallelisation
You may run multiple, differently configured binaries in independent subfolders under independent terminals.

## Output
Results will be generated in sub-subfoldes named e.g. `results0` ... `results4` 
